<?php

/**

 * The base configuration for WordPress

 *

 * The wp-config.php creation script uses this file during the

 * installation. You don't have to use the web site, you can

 * copy this file to "wp-config.php" and fill in the values.

 *

 * This file contains the following configurations:

 *

 * * MySQL settings

 * * Secret keys

 * * Database table prefix

 * * ABSPATH

 *

 * @link https://wordpress.org/support/article/editing-wp-config-php/

 *

 * @package WordPress

 */


// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */

define( 'DB_NAME', '' );


/** MySQL database username */

define( 'DB_USER', '' );


/** MySQL database password */

define( 'DB_PASSWORD', '' );


/** MySQL hostname */

define( 'DB_HOST', '' );


/** Database Charset to use in creating database tables. */

define( 'DB_CHARSET', 'utf8mb4' );


/** The Database Collate type. Don't change this if in doubt. */

define( 'DB_COLLATE', '' );


/**#@+

 * Authentication Unique Keys and Salts.

 *

 * Change these to different unique phrases!

 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}

 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.

 *

 * @since 2.6.0

 */

define( 'AUTH_KEY',         '[ZuvJTZ~$_3El}.W.bRW2,G99}X?NN(;a=*_>H]^1<chE=D=JL3g31;ySX0r I&9' );

define( 'SECURE_AUTH_KEY',  's!j{Ag!&a+1EB6bb?`g5f[V/8(TX^XWS%1>?p4Ck6.3V{6_o+FjRG>5qhiJH+I[E' );

define( 'LOGGED_IN_KEY',    'mymRu{jUC<pzFW=[VekfVi|%|.~2dral`$HXQvO%!G>hYfotBO!)[>{D+~FQOgn{' );

define( 'NONCE_KEY',        'p_qXp3@U/X <U_**!p.Xc`Q[tUP4.?BXXYNm,/a~U~!-G){d@TV/1_c_O7E6o~De' );

define( 'AUTH_SALT',        '/:Iw(U<5XKcn=E6mLa:P,W} WQmI./uDAqJD~OMugMesC.}4[rF<EBcZ|Tj)n,wp' );

define( 'SECURE_AUTH_SALT', 'HcYJzYWZx4R$dU7A^)%YWr6^f{q uT`~DbPApvSN~4fYLGD3nBLei|((<1jUp{Gk' );

define( 'LOGGED_IN_SALT',   'UjtK#TZtXlG=[kk9}9I@osFWBl]6|3Ph8@>0(LgSa`i[1)3Q8O^aBo//^k:gh#_m' );

define( 'NONCE_SALT',       ';rgM/7Ix@o:{|HkN/#S>$i4Z,?M=?;EKe*&eMLOtK>Jp1n;X &N*Q+;?v<#dkXjd' );


/**#@-*/


/**

 * WordPress Database Table prefix.

 *

 * You can have multiple installations in one database if you give each

 * a unique prefix. Only numbers, letters, and underscores please!

 */

$table_prefix = 'wp_';


/**

 * For developers: WordPress debugging mode.

 *

 * Change this to true to enable the display of notices during development.

 * It is strongly recommended that plugin and theme developers use WP_DEBUG

 * in their development environments.

 *

 * For information on other constants that can be used for debugging,

 * visit the documentation.

 *

 * @link https://wordpress.org/support/article/debugging-in-wordpress/

 */

define( 'WP_DEBUG', false );


/* That's all, stop editing! Happy publishing. */


/** Absolute path to the WordPress directory. */

if ( ! defined( 'ABSPATH' ) ) {

	define( 'ABSPATH', __DIR__ . '/' );

}


/** Sets up WordPress vars and included files. */

require_once ABSPATH . 'wp-settings.php';

